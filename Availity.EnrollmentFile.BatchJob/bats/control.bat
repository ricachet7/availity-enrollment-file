SET DB_SERVER_NAME=(localdb)\MSSQLLocalDB
SET DB_NAME=AvailtyEnrollment
SET DB_DefaultFilePrefix=AvailtyEnrollment
SET DB_DefaultDataPath=C:\Users\ricac\AppData\Local\Microsoft\Microsoft SQL Server Local DB\Instances\MSSQLLocalDB\
SET DB_DefaultLogPath=C:\Users\ricac\AppData\Local\Microsoft\Microsoft SQL Server Local DB\Instances\MSSQLLocalDB\
SET DATA_FILE_PATH=C:\Users\ricac\source\repos\availity\availity-enrollment-file\Availity.EnrollmentFile.TestData\myfavoritemanagementsystem-1.csv
SET DATA_FILE_DELIMITER=,
SET FIRST_ROW=2
SET EXPORT_DIR_PATH=C:\Users\ricac\source\repos\availity\availity-enrollment-file\Availity.EnrollmentFile.BatchJob\exports

MKDIR ..\cli-output
MKDIR %EXPORT_DIR_PATH%

call init-db.bat
call clear-temp-table.bat
call import-data-file.bat
call move-import-data-to-primary-table.bat
call generate-export-enrollment-data-bat-file.bat
call export-enrollment-data.bat
