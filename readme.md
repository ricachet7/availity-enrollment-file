Description
======================
Availity receives enrollment files from various benefits management and enrollment solutions (I.e. HR platforms, payroll platforms).  Most of these files are typically in EDI format.  However, there are some files in CSV format.  For the files in CSV format, write a program in a language that seems appropriate to you that will read the content of the file and separate enrollees by insurance company in its own file. Additionally, sort the contents of each file by last and first name (ascending).  Lastly, if there are duplicate User Ids for the same Insurance Company, then only the record with the highest version should be included. The following data points are included in the file:
•	User Id (string)
•	First and Last Name (string)
•	Version (integer)
•	Insurance Company (string)

Dependencies
=======================
MS Command Line Utilities - https://go.microsoft.com/fwlink/?linkid=2142258
MS LocalDB - https://docs.microsoft.com/en-us/sql/database-engine/configure-windows/sql-server-express-localdb?view=sql-server-ver15#installation-media

Examples
============================
Open ".\Availity.EnrollmentFile.BatchJob\bats\control.bat", update variables as needed, and save.
Variable Descriptions:
	- DB_SERVER_NAME: Name of database server. Ex. (localdb)\MSSQLLocalDB
	- DB_NAME: Name of database. Ex. AvailtyEnrollment
	- DB_DefaultFilePrefix: Named to be used as prefix for database .ldf and .mdf files. Ex. AvailtyEnrollment
	- DB_DefaultDataPath: Path to directory that will contain .mdf file.
	- DB_DefaultLogPath: Path to directory that will contain .ldf file.
	- DATA_FILE_PATH: Path to .csv file to be processed.
	- DATA_FILE_DELIMITER: Character used to separate data elements in file to be processed. Ex. ,
	- FIRST_ROW: Row were data starts in file to be processed. Allows process to skip header row if present.
	- EXPORT_DIR_PATH: Path to place insurance company enrollee .csv files.
Open command prompt, navigate to directory containing control.bat, and execute (or navigate to control.bat in file explorer and double click it).