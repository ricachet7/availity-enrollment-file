﻿CREATE PROCEDURE [dbo].[sp_MoveDataFromTempToPrimary]
	
AS

INSERT INTO dbo.t_Enrollment(userId, firstName, lastName, [version], insuranceCompany)
	SELECT s.userId, s.firstName, s.lastName, s.[version], s.insuranceCompany
		FROM (
			SELECT DISTINCT userId = LTRIM(RTRIM(userId)), firstName = LTRIM(RTRIM(firstName)), lastName = LTRIM(RTRIM(lastName))
					, [version] = LTRIM(RTRIM([version])), insuranceCompany = LTRIM(RTRIM(insuranceCompany))
				FROM [dbo].[t_TempEnrollment]
				WHERE NULLIF(userId, '') IS NOT NULL
					AND NULLIF(firstName, '') IS NOT NULL
					AND NULLIF(lastName, '') IS NOT NULL
					AND NULLIF([version], '') IS NOT NULL
					AND ISNUMERIC([version]) = 1
					AND NULLIF(insuranceCompany, '') IS NOT NULL
		) s
		LEFT JOIN dbo.t_Enrollment e ON s.userId = e.userId
									AND s.[version] = e.[version]
									AND s.insuranceCompany = e.insuranceCompany
		WHERE e.id IS NULL

TRUNCATE TABLE [dbo].[t_TempEnrollment]