﻿CREATE PROCEDURE [dbo].[sp_ExportEnrollmentData]
	@icName VARCHAR(250)
AS
	SELECT e.userId, e.firstName, e.lastName, e.[version], e.insuranceCompany
		FROM dbo.t_Enrollment e
			INNER JOIN (
				SELECT userId, [version] = MAX([version])
					FROM dbo.t_Enrollment
					WHERE insuranceCompany = @icName
					GROUP BY userId
			) n ON e.userId = n.userId
				AND e.[version] = n.[version] 
		WHERE e.insuranceCompany = @icName
		ORDER BY e.lastName, e.firstName