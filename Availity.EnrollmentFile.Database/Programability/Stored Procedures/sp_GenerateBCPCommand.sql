﻿CREATE PROCEDURE [dbo].[sp_GenerateBCPCommand]

@outputPath VARCHAR(250)

AS

DECLARE @tCMD TABLE(
	i INT NOT NULL,
	ic VARCHAR(250) NOT NULL,
	cmd VARCHAR(8000) NOT NULL
)

DECLARE @tICs TABLE(ic VARCHAR(250) NOT NULL, PRIMARY KEY(ic))

SET @outputPath = REPLACE(@outputPath, '"', '')

IF(RIGHT(@outputPath, 1) = '\')
	SET @outputPath = SUBSTRING(@outputPath, 1, LEN(@outputPath) - 1)

INSERT INTO @tICs
	SELECT DISTINCT insuranceCompany
		FROM dbo.t_Enrollment

INSERT INTO @tCMD
	SELECT 0, ic, 'ECHO userId,firstName,lastName,version,insuranceCompany>"' + @outputPath + '\' + LOWER([dbo].[fx_CleanStringForUseInfoFilePath](ic)) + '-' + CONVERT(VARCHAR, GETDATE(), 112) + '.csv"'
		FROM @tICs
	UNION
	SELECT 1, ic, 'bcp "EXEC dbo.sp_ExportEnrollmentData ''' + ic + '''" queryout "' + @outputPath + '\temp.data" -c -t "," -T -S %DB_SERVER_NAME% -d %DB_NAME% -o "..\cli-output\export-enrollment-data-' + LOWER([dbo].[fx_CleanStringForUseInfoFilePath](ic)) + '.txt"'
		FROM @tICs
	UNION
	SELECT 2, ic, 'TYPE "' + @outputPath + '\temp.data">>"' + @outputPath + '\' + LOWER([dbo].[fx_CleanStringForUseInfoFilePath](ic)) + '-' + CONVERT(VARCHAR, GETDATE(), 112) + '.csv"'
		FROM @tICs
	UNION
	SELECT 2, 'zzzzzzzzzzzzzzzzzzzzzzzzz', 'DEL "' + @outputPath + '\temp.data"'

SELECT cmd
	FROM @tCMD
	ORDER BY ic, i