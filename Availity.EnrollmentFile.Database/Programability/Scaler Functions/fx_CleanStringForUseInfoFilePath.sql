﻿CREATE FUNCTION [dbo].[fx_CleanStringForUseInfoFilePath]
(
	@input VARCHAR(250)
)
RETURNS VARCHAR(250)
AS
BEGIN
	DECLARE @curC CHAR
	DECLARE @tInvalidPathCharacters TABLE(c CHAR NOT NULL, descption VARCHAR(250))

	INSERT INTO @tInvalidPathCharacters
		VALUES( '<','(less than)')
			  ,('>','(greater than)')
			  ,(':','(colon - sometimes works, but is actually NTFS Alternate Data Streams)')
			  ,('"','(double quote)')
			  ,('/','(forward slash)')
			  ,('\','(backslash)')
			  ,('|','(vertical bar or pipe)')
			  ,('?','(question mark)')
			  ,('*','(asterisk)')
			  ,(' ','(blank space)')
			  ,('[','(open bracket)')
			  ,(']','(closing bracket)')
			  ,('(','(open parentheses)')
			  ,(')','(closing parentheses)')

	WHILE(EXISTS(SELECT TOP 1 * FROM @tInvalidPathCharacters))
	BEGIN
		SELECT TOP 1 @curC = c
			FROM @tInvalidPathCharacters

		IF(CHARINDEX(@curC, @input) <> 0)
			SET @input = REPLACE(@input, @curC, '')

		DELETE FROM @tInvalidPathCharacters WHERE c = @curC
	END

	RETURN @input
END
