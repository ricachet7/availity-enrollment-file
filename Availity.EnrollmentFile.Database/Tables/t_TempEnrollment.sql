﻿CREATE TABLE [dbo].[t_TempEnrollment]
(
	userId VARCHAR(255),
	firstName VARCHAR(255),
	lastName VARCHAR(255),
	[version] VARCHAR(255),
	insuranceCompany VARCHAR(255)
)
