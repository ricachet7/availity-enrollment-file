﻿CREATE TABLE [dbo].[t_Enrollment]
(
	id INT NOT NULL IDENTITY(1,1) PRIMARY KEY CLUSTERED,
	userId VARCHAR(250) NOT NULL,
	firstName VARCHAR(80) NOT NULL,
	lastName VARCHAR(80) NOT NULL,
	[version] INT NOT NULL,
	insuranceCompany VARCHAR(250) NOT NULL,
	CONSTRAINT UQ_dbo_t_Enrollment UNIQUE (userId, [version], insuranceCompany)
)
